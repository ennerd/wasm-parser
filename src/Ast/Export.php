<?php
namespace F2\Wasm\Ast;

abstract class Export extends HelperNode {

    const KIND = null;

    const KIND_FUNCTION = 0x00;
    const KIND_TABLE    = 0x01;
    const KIND_MEMORY   = 0x02;
    const KIND_GLOBAL   = 0x03;

    protected $identifier, $index;

    public function __construct(string $identifier, int $index) {
        $this->identifier = $identifier;
        $this->index = $index;
    }

    public function getIdentifier():string {
        return $this->identifier;
    }

    public function getIndex():int {
        return $this->index;
    }

}
