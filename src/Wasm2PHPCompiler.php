<?php
declare(strict_types=1);

class Wasm2PHPCompiler {

    protected $module;

    public function __construct(WasmToken $module) {
        $this->module = $module;
    }

    public function compile(string $className):string {
        $code = <<<EOD
class $className {
EOD;

        $code .= $this->compileProperties();

        $code .= <<<EOD
}
EOD;
        return $code;
    }

    protected function compileProperties():string {
var_dump($this->module);
        $code = <<<EOD

    const F2_COMPILER = 'f2/compiler';

EOD;
        return $code;
    }
}
