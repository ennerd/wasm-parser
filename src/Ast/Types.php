<?php
namespace F2\Wasm\Ast;

class Types extends Section {

    const TYPE = Node::TYPE_TYPES;

    public function __construct(array $types) {
        $this->types = [];
        foreach($types as $type) {
            $this->types[] = $type->getAst();
        }
    }

}
