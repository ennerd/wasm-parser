<?php

/*
    public function readFloat32():float {
        $bytes = fread($this->fp, 4);
        $sign = ord($bytes[0]) >> 7;
    }
*/

// 47D9F95E = 111602.734375
// C4028000 = -522
// 457F9000 = 4089
// 2D7F5 = 6.00804264307E-39

function intToFloat32(int $v):float {
    $x = ($v & ((1 << 23) - 1)) + (1 << 23) * ($v >> 31 | 1);
    $s = ($v >> 31 & 0b1) ? -1 : 1;
    $exp = ($v >> 23 & 0xFF) - 127;
    return $s * $x * pow(2, $exp - 23);


    echo str_pad(decbin($v), 32, "0", STR_PAD_LEFT)."\n";
    if ($v === 0) return 0.0;
    if ($v === 0x80000000) return -0.0;


    $sign = -1 * ($v >> 31 & 0b1);
    $exp = $v >> 23 & 0xFF;
    $mantissa = 1 / (($v & 0b11111111111111111111111) >> 1);
    echo "EXP: ".pow(2, $exp-0x7F)."\n";
    echo "MANTISSA: ".$mantissa."\n";
    var_dump(pow(2, $exp-0x7F) * $mantissa);

var_dump(decbin($mantissa));
die();
    $mantissa = 1 + ($mantissa / 0xFFFFFF);


var_dump("MANTISSA: ".$mantissa);
//    $mantissa = 0x8FFFFF / 2**($v & 0b11111111111111111111111);

    echo dumpByte($sign)." ".dumpByte($exp)." $mantissa=".decbin($mantissa)."\n";

    return pow(2, $exp-0x7F); // * ($mantissa / 0x8000000);

    echo dumpByte($sign)." ".dumpByte($exp)."\n";

    $a = $v >> 24 & 0xFF;
    $b = $v >> 16 & 0xFF;
    $c = $v >> 8 & 0xFF;
    $d = $v & 0xff;







    echo dumpByte($a)." ".dumpByte($b)." ".dumpByte($c)." ".dumpByte($d)."\n";

    $exp = ($v >> 23) & 0xEF;
    $mantissa = $v & 0x7FFFFF;

    echo "Sign: $sign Mantissa: $mantissa Exp: $exp\n";
    echo
        "Sign: ".str_pad(decbin($sign), 1, "0", STR_PAD_LEFT)."\n".
        "Exp: ".str_pad(decbin($exp), 8, "0", STR_PAD_LEFT)."\n".
        "Mantissa: ".str_pad(decbin($mantissa), 23, "0", STR_PAD_LEFT)."\n";
die();
//    $x = ($v & ((1 << 23) - 1)) + (1 << 23) * ($v >> 31 | 1);
//    $exp = ($v >> 32 & 0xFF) - 127;
    return $x * pow(2, $exp - 23);
}

echo "EXPECT +1.0: ";
var_dump(intToFloat32(0x3F800000));
echo "\n\nEXPECT +2.0: ";
var_dump(intToFloat32(0x40000000));

echo "\n\nEXPECT -17.0: ";
var_dump(intToFloat32(0xC1880000));

echo "\n\nEXPECT 17.0: ";
var_dump(intToFloat32(0x41880000));


die("\n\nDONE\n");

var_dump(0xffffffff | 0x80000000);


function dumpByte($byte) {
    return "$byte=".dechex($byte)."=".str_pad(decbin($byte), 8, "0", STR_PAD_LEFT);
}
