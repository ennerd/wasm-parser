<?php
namespace F2\Wasm\Ast;

class Func extends Type {
    const TYPE = Node::TYPE_FUNC;

    protected $params = [];
    protected $returns = [];

    public function __construct(array $params, array $returns) {
        foreach($params as $param) {
            $this->params[] = $param->getAst();
        }
        foreach($returns as $ret) {
            $this->returns[] = $ret->getAst();
        }
    }

    public function getParams():iterable {
        return $this->params;
    }

    public function getReturns():iterable {
        return $this->returns;
    }
}
