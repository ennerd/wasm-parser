<?php
namespace F2\Wasm\Ast;

use F2\Wasm\Exception;

/**
 * This node type has no significance in the WASM binary. It's simply here
 * to provide classification.
 */
class HelperNode extends Node {

    const TYPE = null;

    public function getType():int {
        throw new Exception("This node type does not have a type");
    }

    public function dump():string {
        return "helpernode\n";
    }

}
