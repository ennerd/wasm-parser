<?php
namespace F2\Wasm\Ast;

class Funcs extends Section {

    const TYPE = Node::TYPE_FUNCS;

    protected $functions = [];

    public function __construct(array $functions) {
        foreach($functions as $index) {
            $this->functions[] = $index;
        }
    }

    public function getFunctionIndexes():iterable {
        return $this->functions;
    }

}

