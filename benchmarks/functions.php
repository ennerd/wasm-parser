<?php
declare(strict_types=1);

function inc_byref(int &$value) {
    $value++;
}

function inc_byref_array(&$data, $offset) {
    $data[$offset]++;
}

function inc_return(int $value):int {
    return 1 + $value;
}
