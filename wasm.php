<?php
require("vendor/autoload.php");
//require("WasmModule.php");
//require("Wasm2PHPCompiler.php");

$parser = new \F2\Wasm\Parser(new \F2\Wasm\BinaryFile('wasm/hello-world.wasm'));

$ast = $parser->getAst();

echo $ast->dump();
