<?php
namespace F2\Wasm\Ast;

class FunctionBody extends HelperNode {
    protected $locals = [];
    protected $instructions = [];

    public function __construct(array $locals, array $instructions) {
        foreach($locals as $local) {
            $this->locals[] = new Local($local);
        }
        $this->instructions = $instructions;
    }

    public function getLocals():iterable {
        return $this->locals;
    }

    public function getInstructions():iterable {
        return $this->instructions;
    }
}
