<?php
declare(strict_types=1);

/**
 * Fast operations:
 *
 * Import the array to your scope:
 *     $data = &$memory->data;
 *
 * Ignore overflows when writing to memory. They are fixed whenever the memory is toStringed.
 *
 *     $data[0]++; // no need to check for overflows
 *
 * Don't ignore overflows when reading from memory, unless you know it can be ignored for your particular case:
 *
 *     $value = $data[0] & 255;
 *
 */


class Memory {

    // Allow public access for performance
    public $data;

    public function __construct($string) {
        $l = strlen($string);
        // Allocate everything, because it is much faster
        $this->data = array_fill(0, $l, 0);
        for($i = 0; $i < $l; $i++) {
            $this->data[$i] = ord($string[$i]);
        }
    }

    public function __toString() {
        return implode("", array_map('chr', $this->data));
    }
}
