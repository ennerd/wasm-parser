<?php
namespace F2\Wasm\Ast;

abstract class Node {

    const TYPE = null;

    const TYPE_MODULE       = 0x6d736100; // 0x0061736d;
    const TYPE_CUSTOM       = 0x00;
    const TYPE_TYPES        = 0x01; //'types';
    const TYPE_IMPORTS      = 0x02; //'imports';
    const TYPE_FUNCS        = 0x03; //'funcs';
    const TYPE_TABLES       = 0x04;
    const TYPE_MEMS         = 0x05;
    const TYPE_GLOBALS      = 0x06;
    const TYPE_EXPORTS      = 0x07;
    const TYPE_START        = 0x08;
    const TYPE_ELEM         = 0x09;
    const TYPE_CODE         = 0x0a;
    const TYPE_DATA         = 0x0b;
    const TYPE_I32          = -0x01; //'i32';
    const TYPE_I64          = -0x02; //'i64';
    const TYPE_F32          = -0x03; //'f32';
    const TYPE_F64          = -0x04; //'f64';
    const TYPE_ANYFUNC      = -0x10; //'anyfunc';
    const TYPE_FUNC         = -0x20; //'func';
    const TYPE_EBT          = -0x40; //'empty block type';

    const LITERALS = [
        self::TYPE_MODULE   => 'module',

        self::TYPE_CUSTOM   => 'customsec',
        self::TYPE_TYPES    => 'types',
        self::TYPE_IMPORTS  => 'imports',
        self::TYPE_FUNCS    => 'funcs',
        self::TYPE_TABLES   => 'tables',
        self::TYPE_MEMS     => 'mems',
        self::TYPE_GLOBALS  => 'globals',
        self::TYPE_EXPORTS  => 'exports',
        self::TYPE_START    => 'start',
        self::TYPE_ELEM     => 'elem',
        self::TYPE_CODE     => 'code',
        self::TYPE_DATA     => 'data',

        self::TYPE_I32      => 'i32',
        self::TYPE_I64      => 'i64',
        self::TYPE_ANYFUNC  => 'anyfunc',
        self::TYPE_FUNC     => 'func',

        self::TYPE_EBT      => 'block_type',
    ];

    public function getType():int {
        return static::TYPE;
    }

    public function getLiteral():string {
        return self::LITERALS[$this->getType()];
    }

    public function dump():string {
        $res = $this->getLiteral().":\n";
        $ref = new \ReflectionClass($this);
        foreach($ref->getProperties() as $prop) {
            $res .= "    ".$prop->getName()."\n";
            $name = $prop->getName();
            $val = $this->$name;
            if(is_iterable($val)) {
                foreach($val as $v) {
                    if (is_int($v)) {
                        $res .= '        '.static::LITERALS[$v]."\n";
                    } else {
                        $res .= static::indent($v->dump(), 8);
                    }
                }
            } else {
                if(is_int($val)) {
                    $res .= static::indent(static::LITERALS[$val], 8);
                } else {
                    $res .= static::indent($val->dump(), 8);
                }
            }
        }
        return $res;
    }

    protected static function indent(string $string, int $spaces):string {
        $lines = explode("\n", $string);
        foreach($lines as &$line) {
            $line = str_repeat(" ", $spaces).$line;
        }
        return implode("\n", $lines);
    }
}
