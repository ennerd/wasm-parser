<?php
declare(strict_types=1);

class WasmModule {

    protected $file;
    protected $header;
    protected $version;
    protected $ast;

    public function __construct(string $path) {
        $this->file = new BinaryFile($path);

        $parser = new WasmTokenParser($this->file);
        $this->ast = $parser->getAst();

        $compiler = new Wasm2PHPCompiler($this->ast);
        echo $compiler->compile('HelloWorld');
    }
}
