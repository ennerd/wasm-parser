<?php
namespace F2\Wasm\Ast;

class Code extends Section {

    const TYPE = Node::TYPE_CODE;

    protected $functions = [];

    public function __construct(array $functions) {
        foreach($functions as $f) {
            $this->functions[] = new FunctionBody($f['locals'], $f['instructions']);
        }
    }

    public function getFunctions():iterable {
        return $this->functions;
    }

}
