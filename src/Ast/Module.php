<?php
namespace F2\Wasm\Ast;

class Module extends Node {

    const TYPE = Node::TYPE_MODULE;

    protected $version;
    protected $sections;

    public function __construct(int $version, array $sections) {
        $this->version = $version;
        $this->sections = [];
        foreach($sections as $section) {
            $this->sections[] = $section->getAst();
        }
    }

    public function getVersion():int {
        return $this->version;
    }

    public function getSections():iterable {
        return $this->sections;
    }
}
