<?php
declare(strict_types=1);
namespace F2\Wasm;

use F2\Wasm\Ast\Node;


/**
 * The Type Section consists of an array of function signatures.
 */

// https://github.com/sunfishcode/wasm-reference-manual/blob/master/WebAssembly.md#type-section

class Parser implements \JsonSerializable {

    public $type = null;
    protected $file;
    protected $extra = [];
/*
    public $param_count = null;
    public $param_types = null;
    public $return_count = null;
    public $return_types = null;
*/
    public function __construct(BinaryFile $file, Parser $parent=null, int $expectType=null) {
        if ($parent === null) {
            // Verify that this is a module
            $this->type = $file->readUint32();
            if ($this->type !== Node::TYPE_MODULE) {
                throw new Exception("Not a module");
            }
            //echo $this->dumpType($this->type)."\n";
            $this->version = $file->readUint32();
            if ($this->version !== 1) {
                throw new Exception("Only supports Wasm 1.0");
            }

            $sections = [];
            while (!$file->isEof()) {
                $sections[] = new static($file, $this);
            }
            $this->sections = $sections;
            return;
        }

        $this->file = $file;
        $type = $file->readVarInt32();
        $this->type = $type;
        //echo $this->dumpType($this->type)."\n";

        if ($expectType !== null & $expectType !== $this->type) {
            throw new Exception("Expected type '".Node::LITERALS[$expectType]."' (0x".dechex($expectType).") but got 0x".dechex($this->id));
        }



        switch($type) {
            case Node::TYPE_TYPES:
                $this->size = $this->file->readVarUInt32();
                $this->types = $this->parseVector(Node::TYPE_FUNC);
                break;

            case Node::TYPE_FUNCS:
                $this->size = $this->file->readVarUInt32();
                $this->funcs = $this->parseArray(function() {
                    return $this->file->readVarUInt32();
                });
                break;

            case Node::TYPE_EXPORTS:
                $this->size = $this->file->readVarUInt32();
                $this->exports = $this->parseArray(function() {
                    return [
                        'identifier' => $this->parseString(),
                        'kind' => $this->file->readVarUInt7(),
                        'index' => $this->file->readVarUInt32(),
                    ];
                });
                break;

            case Node::TYPE_CODE:
                $this->size = $this->file->readVarUInt32();
                $this->functions = $this->parseArray(function() {
                    $bodySize = $this->file->readVarUInt32();
                    $offset = $this->file->tell();
                    $locals = $this->parseArray(function() {
                        throw new Exception("LEARN TO PARSE LOCALS!");
                    });
                    $opcodeCount = $bodySize - ($this->file->tell() - $offset);
                    $opcodes = [];
                    for($i = 0; $i < $opcodeCount; $i++) {
                        $opcodes[] = $this->file->readInt8();
                    }
                    return ['locals' => $locals, 'instructions' => $opcodes];
                });
                break;
            case Node::TYPE_I32:
                break;
            case Node::TYPE_I64:
                break;
            case Node::TYPE_F32:
                break;
            case Node::TYPE_F64:
                break;
            case Node::TYPE_ANYFUNC:
                throw new \Exception("I don't understand anyfunc yet!");
                break;
            case Node::TYPE_FUNC:
                $this->params = $this->parseArray(function() {
                    return new static($this->file, $this);
                });
                $this->returns = $this->parseArray(function() {
                    return new static($this->file, $this);
                });
                break;
            default:
                throw new \Exception("Unknown type identifier ".dechex($type));
        }

        //echo "\n";
    }

    public function getAst():Node {
        switch ($this->type) {
            case Node::TYPE_MODULE:
                return new Ast\Module($this->version, $this->sections);
                break;
            case Node::TYPE_TYPES:
                return new Ast\Types($this->types);
            case Node::TYPE_FUNC:
                return new Ast\Func($this->params, $this->returns);
            case Node::TYPE_I32:
                return new Ast\I32();
            case Node::TYPE_F32:
                return new Ast\F32();
            case Node::TYPE_I64:
                return new Ast\I64();
            case Node::TYPE_F64:
                return new Ast\F64();
            case Node::TYPE_FUNCS:
                return new Ast\Funcs($this->funcs);
            case Node::TYPE_EXPORTS:
                return new Ast\Exports($this->exports);
            case Node::TYPE_CODE:
                return new Ast\Code($this->functions);
            default:
                throw new Exception("No exporter for this node type (".static::intHex($this->type).")");
        }
    }

    public function __set($key, $val):void {
        //echo "- ".Node::LITERALS[$this->type].".$key: ".json_encode($val)."\n";
        $this->extra[$key] = $val;
    }

    public function &__get($key) {
        return $this->extra[$key];
    }

    protected function parseString():string {
        $length = $this->file->readVarUInt32();
        $result = '';
        for($i = 0; $i < $length; $i++) {
            $result[$i] = chr($this->file->readInt8());
        }
        return $result;
    }

    protected function parseVector(int $typeId):array {
        $count = $this->file->readVarUInt32();
        //echo "- parsing vector of ".Node::LITERALS[$typeId]." ($count elements)\n";
        $entries = [];
        for($i = 0; $i < $count; $i++) {
            $entries[] = new static($this->file, $this, $typeId);
        }
        return $entries;
//        $this->entries = $entries;
    }

    protected function parseArray(callable $entryParser):array {
        $count = $this->file->readVarUInt32();
        //echo "- parsing array ($count elements)\n";
        $entries = [];
        for($i = 0; $i < $count; $i++) {
            $entries[] = $entryParser();
        }
        return $entries;
    }

    public static function intHex(int $int):string {
        if ($int < 0) {
            $hex = dechex(-$int);
            return "-0x$hex = $int";
        } else {
            return "0x".hexdec($int)." = $int";
        }
    }

    protected function dumpType(int $type):string {
        return $this->dump($type, "type ".Node::LITERALS[$type]);
    }

    protected function dump(int $what, string $name):string {
        return "$name: 0x".dechex($what&0xFFFFFFFF)." ".$what;
        return "$name: 0b".decbin($what&0xFFFFFFFF)." 0x".dechex($what&0xFFFFFFFF)." ".$what;
    }

    public function jsonSerialize() {
        $result = [ Node::LITERALS[$this->type] ];

        foreach($this->extra as $k => $v) {
            $result[] = [ $k, $v ];
        }

        return $result;

        return [ Node::LITERALS[$this->type], $this->extra ];

        return [ 'type' => Node::LITERALS[$this->type] ] + $this->extra;
    }
}
