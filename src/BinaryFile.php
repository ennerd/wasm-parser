<?php
declare(strict_types=1);
namespace F2\Wasm;

class BinaryFile {

    protected $fp;
    protected $filesize;

    public function __construct(string $path) {
        $this->filesize = filesize($path);
        $this->fp = fopen($path, 'rb');
    }

    public function tell():int {
        return ftell($this->fp);
    }

    public function isEof():bool {
        return $this->tell() === $this->filesize;
        return feof($this->fp);
    }

    public function readInt8():int {
        $byte = fread($this->fp, 1);
        if ($byte === "") throw new \Exception("End of file");
        return ord($byte);
    }

    public function readUInt32():int {
        $bytes = fread($this->fp, 4);
        return ord($bytes[0]) | ord($bytes[1])<<8 | ord($bytes[2])<<16 | ord($bytes[3])<<24;
    }

    public function readVarUInt1():int {
        return $this->readVarUInt(1) & 1;
    }

    public function readVarUInt7():int {
        return $this->readVarUInt(1) & 127;
    }

    public function readVarUInt32():int {
        return $this->readVarUInt(4);
    }

    public function readVarUInt64():int {
        return $this->readVarUint(8);
    }

    public function readVarInt7():int {
        return $this->readVarInt(7);
    }

    public function readVarInt32():int {
        return $this->readVarInt(32);
    }

    public function readVarInt64():int {
        return $this->readVarInt(64);
    }

    public function readFloat32():float {
/*
        $f = unpack('g', fread($this->fp, 4));
        return $f[0];
*/
        $v = $this->readUInt32();
        $x = ($v & ((1 << 23) - 1)) + (1 << 23) * ($v >> 31 | 1);
        $s = ($v >> 31 & 0b1) ? -1 : 1;
        $exp = ($v >> 23 & 0xFF) - 127;
        return $s * $x * pow(2, $exp - 23);
    }

    public function readFloat64():float {
        // WATCH OUT! WONT WORK UNLESS MACHINE IS LITTLE ENDIAN SUCH AS INTEL / AMD
        $f = unpack('e', fread($this->fp, 8));
        return $f[0];


        $v = ($this->readUInt32() << 32) | $this->readUInt32();

        $x = ($v & ((1 << 52) - 1)) + (1 << 52) * ($v >> 63 | 1);
        $s = ($v >> 63 & 0b1) ? -1 : 1;
        $exp = ($v >> 52 & 0b11111111111) - 1023;
        return $s * $x * pow(2, $exp - 23);
    }

    // Thanks to https://github.com/TheFox/utilities/blob/master/src/Leb128.php
    protected function readVarUInt($maxLength):int {
        $len = 0;
        $x = 0;
        while (false !== ($char = fread($this->fp, 1))) {
            $char = ord($char);
            //echo "readVarUInt: byte ".dechex($char)."\n";
            $x |= ($char & 0x7f) << (7 * $len);
            $len++;
            if (($char & 0x80) === 0) {
                return $x;
            }

            if ($len >= $maxLength) {
                throw new \RuntimeException("Max length $maxLength reached");
            }
        }
        throw new \RuntimeException("End of file before finished parsing varuint");
    }

    //https://en.wikipedia.org/wiki/LEB128#Signed_LEB128
    protected function readVarInt($bits):int {
        $result = 0;
        $shift = 0;
        $size = $bits;
        do {
            $byte = ord(fread($this->fp, 1));
            //echo "readVarInt: byte ".dechex($byte)."\n";
            $result |= ($byte & 0x7F) << $shift;
            $shift += 7;
        } while ($byte >> 7 !== 0);

        if ( ($shift <= $size) && ($byte & 0x40) ) {
            $result -= 0b1 << $shift;
//            $result |= (0xFF << $shift);
        }
        return $result;


        $len = 0;
        $x = 0;
        while(false !== ($char = fread($this->fp, 1))) {
            $char = ord($char);
            $shift = 7 * $len;
            $x |= ($char & 0x7f) << $shift;
            $len++;

            if(($char & 0x80) === 0) {
                $shift = 7 * $len;
                if ($shift >= 64) {
                    throw new Exception("INTSIZE BIGGER THAN 64. LOOK INTO THIS.");
                }
                if ($shift < 128 && $char & 0x40) {
                    $x |= -(1 << $shift);
                }
                return $x;
            }

            if ($len >= $maxLength) {
                throw new \RuntimeException("Max length $maxLength reached");
            }
        }
        throw new \RuntimeException("End of file before finished parsing varuint");
    }
}
