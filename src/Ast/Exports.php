<?php
namespace F2\Wasm\Ast;

class Exports extends Section {

    const TYPE = Node::TYPE_EXPORTS;

    protected $exports = [];

    public function __construct(array $exports) {
        foreach($exports as $export) {
            switch($export['kind']) {
                case Export::KIND_FUNCTION:
                    $this->exports[] = new FunctionExport($export['identifier'], $export['index']);
                    break;
                case Export::KIND_TABLE:
                    $this->exports[] = new TableExport($export['identifier'], $export['index']);
                    break;
                case Export::KIND_GLOBAL:
                    $this->exports[] = new GlobalExport($export['identifier'], $export['index']);
                    break;
                case Export::KIND_MEMORY:
                    $this->exports[] = new MemoryExport($export['identifier'], $export['index']);
                    break;
                default:
                    throw new Exception("Unknown export kind ".$export['kind']);
                    break;
            }
        }
    }

    public function getExports():iterable {
        return $this->exports;
    }

}

